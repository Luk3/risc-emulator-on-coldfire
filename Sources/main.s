// Project name: Final Project
// Programmers: Luke Shiffer and John Timms
// Language: ColdFire Assembly
// Target: Freescale MCF52259
// Course: EE-357 SP-14

// ----- MEMORY MAP ----- //
//	
//	A0 - Program Counter
//	A1 - Pointer to register RS
//	A2 - Pointer to register RT
//	A3 - Pointer to register RD
//	A4
//	A5
//	A6
//	A7
//
//	D0 - Used for setup and storage of instructions loaded from memory
//	D1 - Value for the shift operations
//	D2 - Value of RS
//	D3 - Value of RT
//	D4 - Value of RD or #IMM
//	D5
//	D6
//	D7
//
// ---------------------- //

// ----- DECLARATIONS ----- //

		.data
		
RAMBASE	.set	0x20000000					// The location in SRAM where the emulator's machine code will begin
RAMVALD	.set	0x00000001					// Used to enable SRAM

// "Registers" for the RISC emulator
// Each register is 32 bits
REG0	.set	0x20000100					// Value is always 0
REG1	.set	0x20000104					//
REG2	.set	0x20000108					//
REG3	.set	0x2000010C					//
REG4	.set	0x20000110					//
REG5	.set	0x20000114					//
REG6	.set	0x20000118					//
REG7	.set	0x2000011C					//

// Op-codes for the instructions
EADD	.set	0b00000001
EADDI	.set	0b00000010
ELOAD	.set	0b00000011
EBE		.set	0b00000100
EBNE	.set	0b00000110
ESUBI	.set	0b00001000
EREADS	.set	0b00110000
EDIS	.set	0b00100000
EEND	.set	0b00000000

// ----- CODE ----- //

		.text
		
		.global _main
		.global main
		
_main:
main:
		/* Clear ColdFire registers */
		CLR.L	D0
		CLR.L	D1
		CLR.L	D2
		CLR.L	D3
		CLR.L	D4
		CLR.L	D5
		CLR.L	D6
		CLR.L	D7
		
		/* Set the switches for input */
		MOVE.L 	#0x0,D0					// Put 0x0 in the D0 register.
		MOVE.B	D0,0x40100074			// Set switch pins for GPIO. (PDDPAR)
		MOVE.B	D0,0x4010002C			// Set switches for input. (DDRDD)
		
		/* Set the LEDs for output */
		MOVE.L 	#0x0,D0					// Put 0x0 in the D0 register.
		MOVE.B 	D0,0x4010006F			// Set LED pins for GPIO. (PTCPAR)
		MOVE.L 	#0xF,D0					// Put 0xF in the D0 register.
		MOVE.B 	D0,0x40100027			// Set LEDs for output. (DDRTC)
		MOVE.L 	#0x0,D0					// Put 0x0 in the D0 register.
		MOVE.L 	D0,0x4010000F			// All LEDs off. (PORTTC)

		/* Initialize SRAM memory */
		MOVE.L	#RAMBASE+RAMVALD,D0		// Load RAMBASE + valid bit into D0
		MOVEC.L	D0,RAMBAR				// Load RAMBAR and enable SRAM.
		
		/* Prep for clearing loop */
		LEA.L	RAMBASE,A0				// Load pointer to first location in SRAM.
		MOVE.L	#512,D0
		
		/* SRAM clearing loop */
clear_ram:
		CLR.L	(A0)+					// Clear 4 bytes of SRAM.
		CLR.L	(A0)+					// Clear 4 bytes of SRAM.
		CLR.L	(A0)+					// Clear 4 bytes of SRAM.
		CLR.L	(A0)+					// Clear 4 bytes of SRAM.
		SUBQ.L	#4,D0					// Decrement loop counter.
		BNE		clear_ram				// Continue until all the SRAM has been initialized to 0.	
		
		/* Prep for loading */
		LEA.L	RAMBASE,A0				// Load pointer to first location in SRAM.
		
		/* Load machine code into SRAM */
load_program:
		MOVE.L	#0b00001000000100000000000000001111,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00000100000001000000000000000000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00000100000011000000000000000000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b11000011000000000000000000000000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00001000010000000000000000001010,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00010010011000000000000000001000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00000111001001000000000000000000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00100000100100000000000000000001,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00011000000111111111111111110100,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b10000001000000001111111111111000,D0
		MOVE.L	D0,(A0)+
		MOVE.L	#0b00000000100100000000000000000001,D0
		MOVE.L	D0,(A0)+
		
		/* Prep for beginning the emulation */
		LEA.L	RAMBASE,A0				// Load pointer to first instruction stored in SRAM
		
		/* Parse the machine code to get the instruction that the program counter (A0) is pointing to */
parse_instruction:
		MOVE.L	(A0),D0					// The instruction goes in D0. The parentheses tell it to read the actual memory.
		MOVEQ.L	#0x1A,D1				// Shift the instruction 26 bits
		LSR.L	D1,D0					// Perform the shift
		CMPI.L	#EADD,D0				// Compare the opcode in D0 to the emulator's ADD opcode
		BEQ		e_add					//
		CMPI.L	#EADDI,D0				// Compare the opcode in D0 to the emulator's ADDI opcode
		BEQ		e_addi					//
		CMPI.L	#ELOAD,D0				// Compare the opcode in D0 to the emulator's LOAD opcode
		BEQ		e_load					//
		CMPI.L	#EBE,D0					// Compare the opcode in D0 to the emulator's BE opcode
		BEQ		e_be					//
		CMPI.L	#EBNE,D0				// Compare the opcode in D0 to the emulator's BNE opcode
		BEQ		e_bne					//
		CMPI.L	#ESUBI,D0				// Compare the opcode in D0 to the emulator's SUBI opcode
		BEQ		e_subi					//
		CMPI.L	#EREADS,D0				// Compare the opcode in D0 to the emulator's READS opcode
		BEQ		e_reads					//
		CMPI.L	#EDIS,D0				// Compare the opcode in D0 to the emulator's DIS opcode
		BEQ		e_dis					//
		CMPI.L	#EEND,D0				// Compare the opcode in D0 to the emulator's END opcode
		BEQ		e_end					//
		BRA		parse_instruction		// This shouldn't ever happen. Enter (what will be) an infinite loop if it does.

//----------------------------//
// ADD
//----------------------------//
	
e_add:
		// Shifts to get the register numbers passed as RS, RT, and RD
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get RD
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3,D4 will be shifted to erase the lower 29 bits to get 3-bit RS,RT,RD (respectively)
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		LSR.L	D1,D4					// D4 now contains the RD register
		
		// Gets the memory addresses from the register numbers passed as RS, RT, and RD
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		MOVE.L	#REG0,A3				// Move the base register (R0) address to A3. We'll add to get the register we want.
		MULU.W	#0x4,D4					// Multiply the RD register by 4 (the register is 32 bits)
		ADDA.L	D4,A3					// A3 is now at the RD register
		
		// Dereferences RS, RT, and RD- their values go in D2, D3, and D4
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		MOVE.L	(A3),D4					// D4 now contains the value in the RD register

		// Perform the actual ADD calculation and store the result in the register passed as RD
		ADD.L	D2,D3					// Add RS to RT. Result is stored where RT used to be.
		MOVE.L	D3,D4					// Put the result in RD like it should be.
		MOVE.L	D4,(A3)					// Store the value in RD. The instruction is now finished.
		
		// ADD does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed

//----------------------------//
// ADD IMMEDIATE
//----------------------------//

e_addi:
		// Shifts to get the register numbers passed as RS and RT and gets #IMM as well
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get #IMM
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3 will be shifted to erase the lower 29 bits to get 3-bit RS,RT.
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the lower 12 bits to get #IMM.
		ASR.L	D1,D4					// D4 now contains #IMM - IMPORTANT: note that it's ASR to preserve the sign
		
		// Gets the memory addresses from the register numbers passed as RS and RT
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		
		// Dereferences RS and RT- their values go in D2 and D3
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		
		// Perform the actual ADDI calculation and store the result in the register passed as RT
		ADD.L	D2,D4					// Add RS to #IMM. Result is stored where #IMM used to be.
		MOVE.L	D4,D3					// Put the result in RT like it should be.
		MOVE.L	D3,(A2)					// Store the value in RT. The instruction is now finished.
		
		// ADDI does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed

//----------------------------//
// LOAD
//----------------------------//

e_load:
		// Shifts to get the register numbers passed as RS and RT and gets #IMM as well
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get #IMM
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3 will be shifted to erase the lower 29 bits to get 3-bit RS,RT.
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the lower 12 bits to get #IMM.
		ASR.L	D1,D4					// D4 now contains #IMM - IMPORTANT: note that it's ASR to preserve the sign
		
		// Gets the memory addresses from the register numbers passed as RS and RT
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		
		// Dereferences RS and RT- their values go in D2 and D3
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		
		// Perform the actual instruction
		// RT gets the value stored in memory at the address RS + #IMM
		// RT is thus equivalent to an emulated "address register"
		MOVE.L	#RAMBASE,D3				// RT contains the first address in memory
		ADD.L	D2,D3					// Add RS to the first address
		ADD.L	D4,D3					// Add #IMM to the first address + RS
		MOVE.L	D3,(A2)					// Store the value in RT. The instruction is now finished.

		// LOAD does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed
	
//----------------------------//
// BRANCH IF EQUAL
//----------------------------//
	
e_be:
		// Shifts to get the register numbers passed as RS and RT and gets #IMM as well
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get #IMM
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3 will be shifted to erase the lower 29 bits to get 3-bit RS,RT.
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the lower 12 bits to get #IMM.
		ASR.L	D1,D4					// D4 now contains #IMM - IMPORTANT: note that it's ASR to preserve the sign of the jump instruction

		// Gets the memory addresses from the register numbers passed as RS and RT
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		
		// Dereferences RS and RT- their values go in D2 and D3
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		
		// Perform the instruction's function
		CMP.L	D2,D3					// Compare the values in RS and RT for equality
		BEQ		e_be_equal				// Branch conditionally

e_be_not:
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed
		
e_be_equal:
		ADD.L	D4,A0					// Increment the program counter by #IMM
		BRA		parse_instruction		// Release control for the next instruction to be performed
	
//----------------------------//
// BRANCH IF NOT EQUAL
//----------------------------//
	
e_bne:
		// Shifts to get the register numbers passed as RS and RT and gets #IMM as well
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get #IMM
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3 will be shifted to erase the lower 29 bits to get 3-bit RS,RT.
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the lower 12 bits to get #IMM.
		ASR.L	D1,D4					// D4 now contains #IMM - IMPORTANT: note that it's ASR to preserve the sign of the jump instruction

		// Gets the memory addresses from the register numbers passed as RS and RT
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		
		// Dereferences RS and RT- their values go in D2 and D3
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		
		// Perform the instruction's function
		CMP.L	D2,D3					// Compare the values in RS and RT for equality
		BEQ		e_bne_equal				// Branch conditionally

e_bne_not:
		ADD.L	D4,A0					// Increment the program counter by #IMM
		BRA		parse_instruction		// Release control for the next instruction to be performed		

e_bne_equal:
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed	
		
//----------------------------//
// SUBTRACT IMMEDIATE
//----------------------------//
	
e_subi:
		// Shifts to get the register numbers passed as RS and RT and gets #IMM as well
		MOVE.L	(A0),D2					// Prepare for shifting to get RS
		MOVE.L	(A0),D3					// Prepare for shifting to get RT
		MOVE.L	(A0),D4					// Prepare for shifting to get #IMM
		MOVEQ.L	#0x6,D1					// Value to shift. D2 will be shifted to erase the 6-bit opcode.
		LSL.L	D1,D2					// Erase 6-bit opcode
		MOVEQ.L	#0x9,D1					// Value to shift. D3 will be shifted to erase the 6-bit opcode + the 3-bit RS
		LSL.L	D1,D3					// Erase 6-bit opcode
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the 6-bit opcode + the 3-bit RS + the 3-bit RT.
		LSL.L	D1,D4					// Erase 6-bit opcode
		MOVEQ.L	#0x1D,D1				// Value to shift. D2,D3 will be shifted to erase the lower 29 bits to get 3-bit RS,RT.
		LSR.L	D1,D2					// D2 now contains the RS register
		LSR.L	D1,D3					// D3 now contains the RT register
		MOVEQ.L	#0xC,D1					// Value to shift. D4 will be shifted to erase the lower 12 bits to get #IMM.
		ASR.L	D1,D4					// D4 now contains #IMM - IMPORTANT: note that it's ASR to preserve the sign
		
		// Gets the memory addresses from the register numbers passed as RS and RT
		MOVE.L	#REG0,A1				// Move the base register (R0) address to A1. We'll add to get the register we want.
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2,A1					// A1 is now at the RS register
		MOVE.L	#REG0,A2				// Move the base register (R0) address to A2. We'll add to get the register we want.
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3,A2					// A2 is now at the RT register
		
		// Dereferences RS and RT- their values go in D2 and D3
		// MUST use size L here - registers are 32 bits long
		MOVE.L	(A1),D2					// D2 now contains the value in the RS register
		MOVE.L	(A2),D3					// D3 now contains the value in the RT register
		
		// Perform the actual SUBI calculation and store the result in the register passed as RT		
		SUB.L	D4,D2					// Subtract #IMM from RS. Result is stored where RS used to be.
		MOVE.L	D2,D3					// Put the result in RT like it should be.
		MOVE.L	D3,(A2)					// Store the value in RT. The instruction is now finished.

		// SUBI does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed
	
//----------------------------//
// READ SWITCHES
//----------------------------//
	
e_reads:
		MOVE.L 	(A0), D3				// Used to Shift for RT
		MOVEQ.L #0x6, D1				// How much to shift
		LSL.L	D1, D3					// Advance the string
		MOVEQ.L	#0x1D, D1				// Erase extra bits
		LSR.L	D1, D3					// D3 now contains RT
		
		MOVE.L	#REG0, A2				//
		MULU.W	#0x4,D3					// Multiply the RT register by 4 (the register is 32 bits)
		ADDA.L	D3, A2					// A2 now points at "D3" in memory
		
		// Get value of switches
		move.b 	0x40100044, d7
		
		// Save it to memory
		lsr.l	#4, d7
		move.l	d7, (A2) 				// Move 32-bits because registers are 32-bits

		// READS does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed
	
//----------------------------//
// DISPLAY
//----------------------------//
	
e_dis:		
		// Get RS
		// In theory, this is actually RT according to the documentation
		MOVE.L 	(A0), D2
		MOVEQ.L	#0x6, D1
		LSL.L	D1, D2
		MOVEQ.L	#0x1D, D1
		LSR.L	D1, D2
		
		MOVE.L	#REG0, A1
		MULU.W	#0x4,D2					// Multiply the RS register by 4 (the register is 32 bits)
		ADDA.L	D2, A1
		
		MOVE.L	(A1), D2				// D2 holds RS now
		
		MOVE.B	D2, 0x4010000F			// Send the value to be displayed...  RS%16?
		
		// DIS does not branch- advance the program counter by 1 and return
		ADDQ.L	#0x4,A0					// Increment the program counter by 1
		BRA		parse_instruction		// Release control for the next instruction to be performed
		
//----------------------------//
// END
//----------------------------//

e_end:
		CLR.L	D0						// Clear D0
		MOVE.B	#0xF,D0					// Move 0b1111 to D0
		MOVE.B	D0,0x4010000F			// Show 1111 on the LEDs

		// This is the end of the program- infinite loop
		BRA		e_end
	


